package fr.umfds.agl.rugby.repositories;

import fr.umfds.agl.rugby.entities.Joueur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface JoueurRepository extends UtilisateurBaseRepository<Joueur> {
}
