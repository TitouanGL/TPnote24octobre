package fr.umfds.agl.rugby.repositories;

import fr.umfds.agl.rugby.entities.Selectionneur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface SelectionneurRepository extends UtilisateurBaseRepository<Selectionneur> {
}
