package fr.umfds.agl.rugby.repositories;

import fr.umfds.agl.rugby.entities.Equipe;
import org.springframework.data.repository.CrudRepository;

public interface EquipeRepository extends CrudRepository<Equipe, Integer> {
}
