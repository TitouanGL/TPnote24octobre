package fr.umfds.agl.rugby.repositories;

import fr.umfds.agl.rugby.entities.Utilisateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface UtilisateurBaseRepository<U extends Utilisateur> extends CrudRepository<U, String> {
    public U findByCourriel(String lastName);
}
