package fr.umfds.agl.rugby.repositories;

import fr.umfds.agl.rugby.entities.Utilisateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface UtilisateurRepository extends UtilisateurBaseRepository<Utilisateur> {

}
