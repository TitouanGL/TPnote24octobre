package fr.umfds.agl.rugby.services;

import fr.umfds.agl.rugby.entities.Utilisateur;
import fr.umfds.agl.rugby.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


@Component
public class SpringDataJpaUserDetailsService implements UserDetailsService {

    private final UtilisateurRepository repository;

    @Autowired
    public SpringDataJpaUserDetailsService(UtilisateurRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Utilisateur user = this.repository.findByCourriel(name);
        System.out.println(user);
        if (user == null) {
            throw new UsernameNotFoundException(name);
        }
        return new User(user.getCourriel(), user.getPassword(),
                AuthorityUtils.createAuthorityList(user.getRole()));
    }

}
