package fr.umfds.agl.rugby.services;

import fr.umfds.agl.rugby.entities.Joueur;
import fr.umfds.agl.rugby.repositories.JoueurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Service
public class JoueurService {
@Autowired
private JoueurRepository joueurRepository;


    public Iterable<Joueur> getJoueurs() {
        return joueurRepository.findAll();
    }

    public Joueur findByCourriel(String courriel){
        return joueurRepository.findByCourriel(courriel);
    }

    public void saveJoueur(Joueur j){
        joueurRepository.save(j);
    }


}
