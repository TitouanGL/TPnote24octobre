package fr.umfds.agl.rugby.controllers;

import fr.umfds.agl.rugby.entities.Joueur;
import fr.umfds.agl.rugby.forms.JoueurForm;
import fr.umfds.agl.rugby.services.JoueurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class JoueurController {
    @Autowired
    private JoueurService joueurService;

    @GetMapping("/listJoueurs")
    public Iterable<Joueur> getJoueurss(Model model) {
        model.addAttribute("joueurs", joueurService.getJoueurs());
        return joueurService.getJoueurs();

    }

    @PreAuthorize("hasRole('ROLE_SELECTIONNEUR')")
    @GetMapping(value = { "/addJoueur" })
    public String showAddJoueurPage(Model model) {
        JoueurForm joueurForm = new JoueurForm();
        model.addAttribute("joueurForm", joueurForm);
        return "addJoueur";
    }

    @PostMapping(value = { "/addJoueur"})
    public String addJoueur(Model model, @ModelAttribute("joueurForm") JoueurForm joueurForm) {
        Joueur j;
        if(joueurService.findByCourriel(joueurForm.getCourriel())!=null){
            j = joueurService.findByCourriel(joueurForm.getCourriel());
            j.setNom(joueurForm.getNom());
            j.setScore(joueurForm.getScore());
        } else {
            j = new Joueur(joueurForm.getNom(), joueurForm.getCourriel(), joueurForm.getScore());
        }
        joueurService.saveJoueur(j);
        return "redirect:/listJoueurs";

    }
}
