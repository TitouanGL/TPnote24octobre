package fr.umfds.agl.rugby.forms;

public class JoueurForm {
    private String courriel;
    private String nom;
    private int score;

    public JoueurForm(String courriel, String nom, int score) {
        this.courriel = courriel;
        this.nom = nom;
        this.score = score;
    }

    public JoueurForm() {
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
