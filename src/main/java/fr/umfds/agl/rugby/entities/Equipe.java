package fr.umfds.agl.rugby.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Equipe {
    @Id
    @GeneratedValue
    private Integer id;
    private LocalDate dateRencontre; // date de la rencontre pour laquelle l'équipe est prévue
    private String adversaire; // adversaire rencontré
    @OneToMany
    private List<Joueur> joueurs=new ArrayList<>(); // les joueurs sélectionnés

    public Equipe(LocalDate dateRencontre, String adversaire) {
        this.dateRencontre = dateRencontre;
        this.adversaire = adversaire;
    }

    public Equipe() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDateRencontre() {
        return dateRencontre;
    }

    public void setDateRencontre(LocalDate dateRencontre) {
        this.dateRencontre = dateRencontre;
    }

    public String getAdversaire() {
        return adversaire;
    }

    public void setAdversaire(String adversaire) {
        this.adversaire = adversaire;
    }

    public void ajoutJoueur(Joueur j){
        if (LocalDate.now().isBefore(dateRencontre)&&!containsJoueur(j)){
            joueurs.add(j);
        }
    }

    public void remplacerJoueur(Joueur sortant, Joueur entrant) throws JoueurNonSelectionneException {
        if (!containsJoueur(sortant)){
            throw new JoueurNonSelectionneException();
        }else{
            ajoutJoueur(entrant);
            joueurs.remove(sortant);
        }
    }

    public int size(){
        return joueurs.size();
    }

    public boolean containsJoueur(Joueur j){
        return joueurs.contains(j);
    }


}
