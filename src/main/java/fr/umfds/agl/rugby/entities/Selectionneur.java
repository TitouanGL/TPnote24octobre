package fr.umfds.agl.rugby.entities;

import javax.persistence.Entity;

@Entity
public class Selectionneur extends Utilisateur{
    public Selectionneur(String nom, String courriel, String password) {
        super(nom, courriel, password, "ROLE_SELECTIONNEUR");
    }

    public Selectionneur() {
        super();
    }
}
