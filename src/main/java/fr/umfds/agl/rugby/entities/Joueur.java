package fr.umfds.agl.rugby.entities;


import fr.umfds.agl.rugby.utils.InteractionsWithFederation;

import javax.persistence.Entity;
import javax.persistence.Transient;


@Entity
public class Joueur extends Utilisateur {

    private int score; // le score du joueur, mis à jour auprès de la fédération
    @Transient
    private InteractionsWithFederation federation; // Instance permettant de communiquer avec la fédération, cet attribut est transient, ie non persistant

    public Joueur(String nom, String courriel, String password, int score, InteractionsWithFederation fede) {
        super(nom, courriel, password, "ROLE_JOUEUR");
        this.score = score;
        this.federation=fede;
    }
    public Joueur(String nom, String courriel, String password, int score) {
        super(nom, courriel, password, "ROLE_JOUEUR");
        this.score = score;
        this.federation = new InteractionsWithFederation();
    }

    public Joueur(String nom, String courriel, int score) {
        super(nom, courriel, "ROLE_JOUEUR");
        this.score = score;
        this.federation = new InteractionsWithFederation();
    }


    public Joueur() {
        super();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /*
    met à jour le score du joueur en appelant le service dédié de la fédération, retourne vrai si le score a été modifié
     */
    public boolean updateScore() {
        int newScore = federation.getScoreofPlayer(getCourriel());
        if (getScore() != newScore) {
            setScore(newScore);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Joueur)) return false;
        if (!super.equals(o)) return false;

        Joueur joueur = (Joueur) o;

        return getScore() == joueur.getScore();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getScore();
        return result;
    }
}