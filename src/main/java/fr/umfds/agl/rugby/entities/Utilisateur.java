package fr.umfds.agl.rugby.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;

@Entity
@Inheritance
public abstract class Utilisateur {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    private String nom;
    @Id
    private String courriel;
    private @JsonIgnore String password;
    private @JsonIgnore String role;

    public Utilisateur() {

    }
    public Utilisateur(String nom, String courriel, String password, String role) {
        this.courriel = courriel;
        setPassword(password);
        this.role = role;
        this.nom = nom;
    }

    public Utilisateur(String nom, String courriel, String role) {
        this.courriel = courriel;
        setPassword(nom);
        this.role = role;
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }



    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur)) return false;

        Utilisateur that = (Utilisateur) o;

        if (!getCourriel().equals(that.getCourriel())) return false;
        if (!getRole().equals(that.getRole())) return false;
        return getNom().equals(that.getNom());
    }

    @Override
    public int hashCode() {
        int result = getCourriel().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getRole().hashCode();
        result = 31 * result + getNom().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "nom='" + nom + '\'' +
                ", courriel='" + courriel + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}