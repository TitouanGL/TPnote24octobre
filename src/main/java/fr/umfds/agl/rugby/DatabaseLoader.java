package fr.umfds.agl.rugby;

import fr.umfds.agl.rugby.entities.Joueur;
import fr.umfds.agl.rugby.entities.Selectionneur;
import fr.umfds.agl.rugby.repositories.JoueurRepository;
import fr.umfds.agl.rugby.repositories.SelectionneurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final JoueurRepository joueurs;
    private final SelectionneurRepository selectionneurs;




    @Autowired
    public DatabaseLoader(JoueurRepository joueurs, SelectionneurRepository selectionneurs) {
        this.joueurs = joueurs;
        this.selectionneurs=selectionneurs;
    }

    @Override
    public void run(String... strings) throws Exception {
        Selectionneur selectionneur=this.selectionneurs.save(new Selectionneur("Stéphane", "stephane@monmail.fr", "123"));
       SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Stéphane", "mdpfictif",
                        AuthorityUtils.createAuthorityList("ROLE_SELECTIONNEUR"))); // the actual password is not needed here
        this.joueurs.save(new Joueur("Dupont", "adupont@monmail.com", "dupont",100));
        this.joueurs.save(new Joueur("Jalibert", "jalibert@monmail.com", "alan",80));
        this.joueurs.save(new Joueur("Serin", "serin@monmail.com", "leslie",90));

        SecurityContextHolder.clearContext();

    }
}
