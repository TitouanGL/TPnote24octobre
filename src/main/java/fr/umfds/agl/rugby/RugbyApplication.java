package fr.umfds.agl.rugby;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class RugbyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RugbyApplication.class, args);
    }

}
